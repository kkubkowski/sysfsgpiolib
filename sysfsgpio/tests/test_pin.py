import unittest
import time

try:
    from ..pin import Pin
    from ..utils import get_pins
except ImportError:
    from sysfsgpio import Pin, get_pins


class TestPinInit(unittest.TestCase):
    PINS = []

    @classmethod
    def setUpClass(cls):
        pins = get_pins()
        try:
            pin = Pin(pins[0][0])
            cls.PINS = pins
        except (IndexError, PermissionError):
            pass

    def test_invalid_idx(self):
        for pin in [-1, '-1', 'a12', '', 400, '400']:
            with self.subTest(pin=pin):
                with self.assertRaises(ValueError):
                    Pin(pin)

        for pin in [[], {}, 3.14]:
            with self.subTest(pin=pin):
                with self.assertRaises(TypeError):
                    Pin(pin)

    def test_pin_idx_and_name_from_idx(self):
        if len(self.PINS) == 0:
            raise unittest.SkipTest('no pins available for testing')
        pin = Pin(self.PINS[0][0])
        self.assertEqual(pin.pin, self.PINS[0][0])
        self.assertEqual(pin.pinname, self.PINS[0][1])

    def test_pin_idx_and_name_from_name(self):
        if len(self.PINS) == 0:
            raise unittest.SkipTest('no pins available for testing')
        pin = Pin(self.PINS[0][1])
        self.assertEqual(pin.pin, self.PINS[0][0])
        self.assertEqual(pin.pinname, self.PINS[0][1])


class TestPin(unittest.TestCase):
    PINS = []

    @classmethod
    def setUpClass(cls):
        try:
            for idx, name in get_pins():
                cls.PINS.append(Pin(name))
        except PermissionError:
            pass

    def __init__(self, *args, **kwargs):
        self.PIN = None
        self.output_done_invoked = False
        super().__init__(*args, **kwargs)

    def setUp(self):
        if len(self.PINS) == 0:
            raise unittest.SkipTest('no pins available for testing')
        self.PIN = self.PINS[0]

    def tearDown(self):
        self.PIN.disable()
        self.PIN = None

    def test_enabled_setter(self):

        with self.assertRaises(TypeError):
            self.PIN.enabled = ''

    def test_direction_setter(self):
        with self.assertRaises(TypeError):
            self.PIN.direction = 3

        with self.assertRaises(ValueError):
            self.PIN.direction = ''

    def test_invert_setter(self):
        with self.assertRaises(TypeError):
            self.PIN.invert = []

        with self.assertRaises(ValueError):
            self.PIN.invert = ''

        self.PIN.invert = True
        self.assertTrue(self.PIN.invert)
        self.PIN.invert = 0
        self.assertFalse(self.PIN.invert)

    def test_value_setter(self):
        with self.assertRaises(TypeError):
            self.PIN.value = []

        with self.assertRaises(ValueError):
            self.PIN.value = ''

        self.PIN.direction = 'out'
        self.PIN.value = 0
        self.assertEqual(self.PIN.value, 0)
        self.PIN.value = 1
        self.assertEqual(self.PIN.value, 1)
        self.PIN.value = 0
        self.assertEqual(self.PIN.value, 0)

    def test_configuration(self):
        for key, value in [('debounce', .125),
                           ('invert', True),
                           ('direction', 'out'),
                           ('ontime', 3.14),
                           ('offtime', 8),
                           ('repeat', 12),
                           ('pattern', [1, 3.14, 2])]:
            self.PIN.configuration = {key: value}
            self.assertEqual(self.PIN.configuration[key], value)

    def test_configuration_change_when_enabled(self):
        self.PIN.direction = 'in'
        self.PIN.enable()
        self.PIN.configuration = dict(debounce=.1,
                                      invert=True,
                                      ontime=1,
                                      offtime=1,
                                      repeat=-1,
                                      pattern=[.1, .5])
        self.assertEqual(self.PIN.configuration['direction'], 'in')
        self.assertEqual(self.PIN.configuration['invert'], True)
        self.assertEqual(self.PIN.configuration['debounce'], .1)
        self.assertEqual(self.PIN.configuration['ontime'], 1)
        self.assertEqual(self.PIN.configuration['offtime'], 1)
        self.assertEqual(self.PIN.configuration['repeat'], -1)
        self.assertEqual(self.PIN.configuration['pattern'], [.1, .5])
        self.PIN.direction = 'out'
        self.assertFalse(self.PIN.enabled)

    def test_value_setter_when_enabled(self):
        self.PIN.direction = 'out'
        self.PIN.ontime = 1
        self.PIN.offtime = 1
        self.PIN.repeat = -1
        self.PIN.enable()
        with self.assertRaises(RuntimeError):
            self.PIN.value = 1
        self.PIN.disable()

    def output_done_callback(self, sender):
        self.output_done_invoked = True

    def test_output_callback(self):
        self.PIN.direction = 'out'
        self.PIN.ontime = .1
        self.PIN.offtime = .1
        self.PIN.repeat = 2
        self.PIN.output_done = self.output_done_callback
        self.PIN.enabled = True
        while self.PIN.enabled:
            time.sleep(.1)
        self.assertTrue(self.output_done_invoked)
        self.output_done_invoked = False


if __name__ == '__main__':
    unittest.main()
